//
//  PostTableCell.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 31.01.2021.
//

import UIKit

class PostTableCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var chanelImage: UIImageView!
    @IBOutlet weak var nickLabel: UILabel!
    @IBOutlet weak var subsLabel: UILabel!
    @IBOutlet weak var textPostLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    
    var id = 0
    var textPost = ""

    override func awakeFromNib() {
        super.awakeFromNib()
        chanelImage.layer.cornerRadius = 16.5
        shareButton.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func seePostTapped(_ sender: Any) {

    }
    
    
}
