//
//  KeyCollectionCell.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 29.01.2021.
//

import UIKit

protocol KeyDelegate {
    func deleteProduct(cell: KeyCollectionCell)
}

class KeyCollectionCell: UICollectionViewCell {
    
    var delegate: KeyDelegate?
    
    @IBOutlet weak var keyTF: UITextField!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func deleteKey(_ sender: Any) {
        self.delegate?.deleteProduct(cell: self)
    }
    
}
