//
//  MyChanelsViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 30.01.2021.
//

import UIKit

class MyChanelsViewController: UIViewController {
    
    @IBOutlet weak var positiveButton: UIButton!
    @IBOutlet weak var positiveView: UIView!
    
    @IBOutlet weak var negativeButton: UIButton!
    @IBOutlet weak var negativeView: UIView!
    @IBOutlet weak var keyTextField: UITextField!
    @IBOutlet weak var keysCollection: UICollectionView!
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var viewButtonsHeight: NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
        goodKeys.removeAll()
        badKeys.removeAll()
        positiveView.isHidden = false
        negativeView.isHidden = true
        negativeButton.setTitleColor(UIColor.gray, for: .normal)
        positiveButton.setTitleColor(UIColor.black, for: .normal)
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/my-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData
        
        struct Keys {
            var id: Int
            var key_id: Int
            var value: String
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
            }
            //print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        //print(json)
                        if let keyOne = json["keyChanel"] as? [[String:Any]]{
                            print("I AM HERE")
                            if !keyOne.isEmpty {
                                print("I AM NOT EMPTY")
                                for number in 0..<keyOne.count {
                                    goodKeys.append(keyOne[number]["url"] as? String ?? "default")
                                }
                            }
                        } else {
                            print("wtf")
                        }
                        if let keyMinus = json["keyChanelBlack"] as? [[String:Any]]{
                            if !keyMinus.isEmpty{
                                for number in 0..<keyMinus.count {
                                    badKeys.append(keyMinus[number]["url"] as? String ?? "default")
                                }
                            }
                        }
                        keys = goodKeys
                        self.keysCollection.reloadData()
                        // try to read out a string array
                        //if let error = json["error"] as? String {
                        //    self.showAlertWithText(error)
                        //}
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
    }

    
    
    override func viewDidLoad() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        super.viewDidLoad()
        keyTextField.text = "Введите название канала"
        let keyCell = UINib(nibName: "KeyCollectionCell",
                                  bundle: nil)
        self.keysCollection.register(keyCell,
                                      forCellWithReuseIdentifier: "KeyCollectionCell")
        keysCollection.delegate = self
        keysCollection.dataSource = self
    }
    
    var isTicked = false
    
    @IBAction func tickTapped(_ sender: Any) {
        if isTicked {
            isTicked = false
            tickButton.setImage(UIImage(systemName: "square"), for: .normal)
        } else {
            isTicked = true
            tickButton.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
        }
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewControllers(viewsToPop: 1)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if isGood {
            if keyTextField.text != "" {
                goodKeys.append(keyTextField.text ?? "")
            }
            keys = goodKeys
            addToGoodKeys(goodKeys)
            keysCollection.reloadData()
        } else {
            if keyTextField.text != "" {
                badKeys.append(keyTextField.text ?? "")
            }
            keys = badKeys
            addToBadKeys(badKeys)
            keysCollection.reloadData()
        }
        keyTextField.text = ""
    }
    
    func addToGoodKeys(_ array: [String]){
        
        var valueString = "["
        
        for number in 0..<array.count {
            valueString += "{\"value\": \"\(array[number])\"}, "
            if number == array.count - 1 {
                valueString += "{\"value\": \"\(array[number])\"}]"
            }
        }

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "keyChanel",
            "value": valueString,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/add-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    func addToBadKeys(_ array: [String]){
        
        var valueString = "["
        
        for number in 0..<array.count {
            valueString += "{\"value\": \"\(array[number])\"}, "
            if number == array.count - 1 {
                valueString += "{\"value\": \"\(array[number])\"}]"
            }
        }

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "keyChanelBlack",
            "value": valueString,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/add-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    @IBAction func goodKeysTapped(_ sender: Any) {
        positiveView.isHidden = false
        negativeView.isHidden = true
        viewButtonsHeight.constant = 30
        viewHeight.constant = 20
        isGood = true
        positiveButton.setTitleColor(UIColor.black, for: .normal)
        negativeButton.setTitleColor(UIColor.gray, for: .normal)
        keys = goodKeys
        keysCollection.reloadData()
    }
    
    @IBAction func badKeysTapped(_ sender: Any) {
        positiveView.isHidden = true
        negativeView.isHidden = false
        viewButtonsHeight.constant = 0
        isTicked = false
        tickButton.setImage(UIImage(systemName: "square"), for: .normal)
        viewHeight.constant = 0
        isGood = false
        positiveButton.setTitleColor(UIColor.gray, for: .normal)
        negativeButton.setTitleColor(UIColor.black, for: .normal)
        keys = badKeys
        keysCollection.reloadData()
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "Очистить список заявок", preferredStyle: .alert)
        let noAction = UIAlertAction(title: "Нет", style: .cancel) { [self] (UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
            
        }
        let clearAction = UIAlertAction(title: "Очистить", style: .default) { [self] (UIAlertAction) in
            goodKeys.removeAll()
            badKeys.removeAll()
            keys.removeAll()
            keysCollection.reloadData()
            self.dismiss(animated: true, completion: nil)
            deleteAll()
        }
        alert.addAction(noAction)
        alert.addAction(clearAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteAll(){

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "keyChanel",
            "value": "[]",
            "type": "text"
          ],
          [
            "key": "keyChanelBlack",
            "value": "[]",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/add-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
    
    
    var isGood = true
    
    var goodKeys = [String]()
    var badKeys = [String]()
    
    var keys = [String]()
}

extension MyChanelsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, KeyDelegate {
    func deleteProduct(cell: KeyCollectionCell) {
        let cellIndex = self.keysCollection.indexPath(for: cell)!.row
        if isGood {
            goodKeys.remove(at: cellIndex)
            keys = goodKeys
            addToGoodKeys(goodKeys)
        } else {
            badKeys.remove(at: cellIndex)
            keys = badKeys
            addToBadKeys(badKeys)
        }
        keysCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return keys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "KeyCollectionCell", for: indexPath) as? KeyCollectionCell)!
        cell.delegate = self
        cell.keyTF.text = keys[indexPath.row]
        cell.layer.cornerRadius = 8
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width - 60
        return CGSize(width: width, height: 40)
    }
}

