//
//  AboutViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 30.01.2021.
//

import UIKit

class AboutViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
