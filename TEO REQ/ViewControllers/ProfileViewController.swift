//
//  ProfileViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 30.01.2021.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    
    override func viewDidLoad() {
        do {
            let url = URL(string: UserDefaults.standard.string(forKey: "avatar") ?? "") ?? URL(string: "https://cdn.icon-icons.com/icons2/1812/PNG/512/4213460-account-avatar-head-person-profile-user_115386.png")
            let data = try Data(contentsOf: url!)
            
            self.profileImage.image = UIImage(data: data)
        }
        catch{
            print(error)
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        super.viewDidLoad()
        profileImage.layer.cornerRadius = 30
        nameLabel.text = UserDefaults.standard.string(forKey: "name") ?? ""
        subNameLabel.text = UserDefaults.standard.string(forKey: "username") ?? ""
        print(UserDefaults.standard.string(forKey: "token") ?? "")
    }
    
    @IBAction func quitTapped(_ sender: Any) {
        UserDefaults.standard.set("" ,forKey: "token")
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func myChanelsTapped(_ sender: Any) {
        print("keykey")
        let vc = storyboard?.instantiateViewController(withIdentifier: "MyChanelsViewController") as! MyChanelsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func aboutTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
