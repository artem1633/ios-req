//
//  FinishRegViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 30.01.2021.
//

import UIKit

class FinishRegViewController: UIViewController {
    
    @IBOutlet weak var nextButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.layer.cornerRadius = 8
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "MainTabBarViewController") as! MainTabBarViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
