//
//  RegKeysViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 29.01.2021.
//

import UIKit

class RegKeysViewController: UIViewController {
    
    @IBOutlet weak var positiveButton: UIButton!
    @IBOutlet weak var negativeButton: UIButton!
    @IBOutlet weak var keyTextField: UITextField!
    @IBOutlet weak var keysCollection: UICollectionView!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var goodKeysView: UIView!
    @IBOutlet weak var badKeysView: UIView!
    
    @IBOutlet weak var buttonHight: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        super.viewDidLoad()
        negativeButton.setTitleColor(UIColor.gray, for: .normal)
        positiveButton.setTitleColor(UIColor.black, for: .normal)
        goodKeysView.isHidden = false
        badKeysView.isHidden = true
        nextButton.layer.cornerRadius = 8
        keyTextField.delegate = self
        keyTextField.text = "Введите название ключевика"
        let keyCell = UINib(nibName: "KeyCollectionCell",
                                  bundle: nil)
        self.keysCollection.register(keyCell,
                                      forCellWithReuseIdentifier: "KeyCollectionCell")
        keysCollection.delegate = self
        keysCollection.dataSource = self
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if buttonHight.constant == 46 {
                buttonHight.constant += keyboardSize.height - 20
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if buttonHight.constant != 46 {
            buttonHight.constant = 46
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewControllers(viewsToPop: 1)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        
        var valueString = "["
        
        for number in 0..<goodKeys.count {
            valueString += "{\"value\": \"\(goodKeys[number])\"}, "
            if number == goodKeys.count - 1 {
                valueString += "{\"value\": \"\(goodKeys[number])\"}]"
            }
        }

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "keyOne",
            "value": valueString,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/add-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }
        
        addBad()

        task.resume()
        
        
        
        
        let vc = storyboard?.instantiateViewController(identifier: "FinishRegViewController") as! FinishRegViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func addBad(){
        var valueString = "["
        
        for number in 0..<badKeys.count {
            valueString += "{\"value\": \"\(badKeys[number])\"}, "
            if number == badKeys.count - 1 {
                valueString += "{\"value\": \"\(badKeys[number])\"}]"
            }
        }

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "keyMinus",
            "value": valueString,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/add-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if isGood {
            if keyTextField.text != "" {
                goodKeys.append(keyTextField.text ?? "")
            }
            keys = goodKeys
            keysCollection.reloadData()
        } else {
            if keyTextField.text != "" {
                badKeys.append(keyTextField.text ?? "")
            }
            keys = badKeys
            keysCollection.reloadData()
        }
        keyTextField.text = ""
    }
    
    @IBAction func goodKeysTapped(_ sender: Any) {
        goodKeysView.isHidden = false
        badKeysView.isHidden = true
        isGood = true
        positiveButton.setTitleColor(UIColor.black, for: .normal)
        negativeButton.setTitleColor(UIColor.gray, for: .normal)
        keys = goodKeys
        keysCollection.reloadData()
    }
    
    @IBAction func badKeysTapped(_ sender: Any) {
        goodKeysView.isHidden = true
        badKeysView.isHidden = false
        isGood = false
        positiveButton.setTitleColor(UIColor.gray, for: .normal)
        negativeButton.setTitleColor(UIColor.black, for: .normal)
        keys = badKeys
        keysCollection.reloadData()
    }
    
    var isGood = true
    
    var goodKeys = [String]()
    var badKeys = [String]()
    
    var keys = [String]()
}

extension RegKeysViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, KeyDelegate {
    func deleteProduct(cell: KeyCollectionCell) {
        let cellIndex = self.keysCollection.indexPath(for: cell)!.row
        if isGood {
            goodKeys.remove(at: cellIndex)
            keys = goodKeys
        } else {
            badKeys.remove(at: cellIndex)
            keys = badKeys
        }
        keysCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return keys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "KeyCollectionCell", for: indexPath) as? KeyCollectionCell)!
        cell.delegate = self
        cell.keyTF.text = keys[indexPath.row]
        cell.layer.cornerRadius = 8
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width - 60
        return CGSize(width: width, height: 40)
    }
}
