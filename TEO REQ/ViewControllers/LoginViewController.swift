//
//  ViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 29.01.2021.
//

import UIKit

extension UIViewController: UITextFieldDelegate {
    func showAlertWithText(_ text: String) {
        let alertWindow = UIAlertController(title: nil, message: text, preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Закрыть", style: .cancel) { (UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        alertWindow.addAction(closeAction)
        self.present(alertWindow, animated: true, completion: nil)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return false
    }
}

class LoginViewController: UIViewController {
    
    @IBOutlet weak var numberTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var goNextButton: UIButton!
    
    override func viewDidLoad() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        super.viewDidLoad()
        goNextButton.layer.cornerRadius = 8
        numberTF.delegate = self
        passwordTF.delegate = self
        numberTF.addTarget(self, action: #selector(self.textFieldDidBegin(_:)), for: .editingDidBegin)
        passwordTF.addTarget(self, action: #selector(self.textFieldDidBegin(_:)), for: .editingDidBegin)
        // Do any additional setup after loading the view.
        numberTF.text = "Телефон"
        passwordTF.text = "Пароль"
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func textFieldDidBegin(_ textField: UITextField) {
        if textField ==  passwordTF {
            textField.isSecureTextEntry = true
            if numberTF.text == "" {
                numberTF.text = "Телефон"
            }
        } else {
            if passwordTF.text == "" {
                passwordTF.isSecureTextEntry = false
                passwordTF.text = "Пароль"
            }
        }
        
    }

    @IBAction func nextTapped(_ sender: Any) {
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "login",
            "value": "\(numberTF.text ?? "")",
            "type": "text"
          ],
          [
            "key": "pas",
            "value": "\(passwordTF.text ?? "")",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/login")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            self.showAlertWithText(String(describing: error))
            semaphore.signal()
            return
          }
            DispatchQueue.main.async { () -> Void in
            do {
                
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        print(json)
                        if json["error"] == nil {
                            UserDefaults.standard.set(json["token"] ,forKey: "token")
                            UserDefaults.standard.set(json["name"] ,forKey: "name")
                            UserDefaults.standard.set(json["username"] ,forKey: "username")
                            if let avatar = json["avatar"] as? String {
                                UserDefaults.standard.set(avatar ,forKey: "avatar")
                            }
                            if let fcm = json["fcm_token"] as? String {
                                self.sendFCM(fcm)
                            } else {
                                self.sendFCM(UserDefaults.standard.string(forKey: "fcm") ?? "")
                            }
                            let vc = self.storyboard?.instantiateViewController(identifier: "MainTabBarViewController") as! MainTabBarViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                        } else {
                            self.showAlertWithText(json["error"] as! String)
                        }
                    // try to read out a string array
                    //if let error = json["error"] as? String {
                    //    self.showAlertWithText(error)
                    //}
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
            }
            semaphore.signal()
        }
        task.resume()
        semaphore.wait()
    }
    
    func sendFCM(_ fcmToken: String){
        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "fcm_token",
            "value": fcmToken,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/fcm")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
    
    @IBAction func forgotTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "CodeViewController") as! CodeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func createTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(identifier: "RegViewController") as! RegViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

class MainTabBarViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

