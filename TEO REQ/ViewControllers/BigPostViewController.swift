//
//  BigPostViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 31.01.2021.
//

import UIKit

protocol PostDelegate {
    func deleteProduct(id: Int)
}

class BigPostViewController: UIViewController {
    
    @IBOutlet weak var chanelImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nickLabel: UILabel!
    @IBOutlet weak var subsLabel: UILabel!
    @IBOutlet weak var postTextTV: UITextView!
    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var chsButton: UIButton!
    
    var imageLink = ""
    var nick = ""
    var name = ""
    var subs = ""
    var textPost = ""
    var openID = ""
    
    var id = 0
    var idPost = ""
    
    var delegate: PostDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        openButton.layer.cornerRadius = 8
        deleteButton.layer.cornerRadius = 8
        chsButton.layer.cornerRadius = 8
        do {
            let url = URL(string: imageLink) ?? URL(string: "https://cdn.icon-icons.com/icons2/1812/PNG/512/4213460-account-avatar-head-person-profile-user_115386.png")
            let data = try Data(contentsOf: url!)
            DispatchQueue.main.async {
                self.chanelImage.image = UIImage(data: data)
            }
        }
        catch{
            print(error)
        }
        nameLabel.text = name
        nickLabel.text = "@" + nick
        subsLabel.text = subs + " чел."
        postTextTV.text = textPost
    }
    
    @IBAction func openTapped(_ sender: Any) {
        guard let url = URL(string: "https://t.me/\(nick)/\(openID)") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        delegate?.deleteProduct(id: id)
        deletePost()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func chsTapped(_ sender: Any) {
        delegate?.deleteProduct(id: id)
        chs()
        self.dismiss(animated: true, completion: nil)
    }
    
    func chs(){
        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/my-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData
        
        struct Keys {
            var id: Int
            var key_id: Int
            var value: String
        }
        
        var badKeys = [String]()

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            //print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        //print(json)
                        if let keyMinus = json["keyChanelBlack"] as? [[String:Any]]{
                            if !keyMinus.isEmpty{
                                for number in 0..<keyMinus.count {
                                    badKeys.append(keyMinus[number]["url"] as? String ?? "default")
                                }
                            }
                        }
                        badKeys.append(name)
                        addToChs(badKeys)
                        // try to read out a string array
                        //if let error = json["error"] as? String {
                        //    self.showAlertWithText(error)
                        //}
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
        
        task.resume()

    }
    
    func addToChs(_ array: [String]){
        var valueString = "["
        
        for number in 0..<array.count {
            valueString += "{\"value\": \"\(array[number])\"}, "
            if number == array.count - 1 {
                valueString += "{\"value\": \"\(array[number])\"}]"
            }
        }

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "keyChanelBlack",
            "value": valueString,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/add-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
    
    func deletePost(){
        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "id",
            "value": idPost,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/del-req")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
        
    }
    
}
