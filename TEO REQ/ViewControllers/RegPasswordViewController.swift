//
//  RegPasswordViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 29.01.2021.
//

import UIKit

class RegPasswordViewController: UIViewController {
    
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var promoTF: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    var name = ""
    var phone = ""
    var email = ""
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        super.viewDidLoad()
        passwordTF.delegate = self
        newPasswordTF.delegate = self
        promoTF.delegate = self
        passwordTF.text = "Пароль"
        newPasswordTF.text = "Подтверждение пароля"
        promoTF.text = "Промокод"
        passwordTF.addTarget(self, action: #selector(self.textFieldDidBegin(_:)), for: .editingDidBegin)
        newPasswordTF.addTarget(self, action: #selector(self.textFieldDidBegin(_:)), for: .editingDidBegin)
        newPasswordTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        nextButton.layer.cornerRadius = 8
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    var canSend = false
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text != passwordTF.text {
            canSend = false
            newPasswordTF.layer.borderWidth = 0.5
            newPasswordTF.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
        } else {
            canSend = true
            newPasswordTF.layer.borderWidth = 0
        }
    }
    
    @objc func textFieldDidBegin(_ textField: UITextField) {
        textField.isSecureTextEntry = true
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewControllers(viewsToPop: 1)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        
        if canSend {
        
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "email",
            "value": email,
            "type": "text"
          ],
          [
            "key": "phone",
            "value": phone,
            "type": "text"
          ],
          [
            "key": "name",
            "value": name,
            "type": "text"
          ],
          [
            "key": "pas",
            "value": passwordTF.text,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/reg")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
            DispatchQueue.main.async { () -> Void in
            do {
                
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        print(json)
                        if json["error"] == nil {
                            UserDefaults.standard.set(json["token"] ,forKey: "token")
                            UserDefaults.standard.set(json["name"] ,forKey: "name")
                            UserDefaults.standard.set(json["username"] ,forKey: "username")
                            if let avatar = json["avatar"] as? String {
                                UserDefaults.standard.set(avatar ,forKey: "avatar")
                            }
                            if let fcm = json["fcm_token"] as? String {
                                self.sendFCM(fcm)
                            } else {
                                self.sendFCM(UserDefaults.standard.string(forKey: "fcm") ?? "")
                            }
                            let vc = self.storyboard?.instantiateViewController(identifier: "RegKeysViewController") as! RegKeysViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                        } else {
                            self.showAlertWithText(json["error"] as! String)
                        }
                    // try to read out a string array
                    //if let error = json["error"] as? String {
                    //    self.showAlertWithText(error)
                    //}
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
            }
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        }
    }
    
    func sendFCM(_ fcmToken: String){
        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "fcm_token",
            "value": fcmToken,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/fcm")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
}
