//
//  KeysViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 29.01.2021.
//

import UIKit

class KeysViewController: UIViewController {
    
    @IBOutlet weak var positiveButton: UIButton!
    @IBOutlet weak var negativeButton: UIButton!
    @IBOutlet weak var keyTextField: UITextField!
    @IBOutlet weak var keysCollection: UICollectionView!
    
    @IBOutlet weak var negativeView: UIView!
    @IBOutlet weak var positiveView: UIView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        goodKeys.removeAll()
        badKeys.removeAll()
        negativeView.isHidden = true
        positiveView.isHidden = false
        negativeButton.setTitleColor(UIColor.gray, for: .normal)
        positiveButton.setTitleColor(UIColor.black, for: .normal)
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/my-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData
        
        struct Keys {
            var id: Int
            var key_id: Int
            var value: String
        }

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
            }
            //print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        //print(json)
                        if let keyOne = json["keyOne"] as? [[String:Any]]{
                            print("I AM HERE")
                            if !keyOne.isEmpty {
                                print("I AM NOT EMPTY")
                                for number in 0..<keyOne.count {
                                    goodKeys.append(keyOne[number]["value"] as? String ?? "default")
                                }
                            }
                        } else {
                            print("wtf")
                        }
                        if let keyMinus = json["keyMinus"] as? [[String:Any]]{
                            if !keyMinus.isEmpty{
                                for number in 0..<keyMinus.count {
                                    badKeys.append(keyMinus[number]["value"] as? String ?? "default")
                                }
                            }
                        }
                        keys = goodKeys
                        self.keysCollection.reloadData()
                        // try to read out a string array
                        //if let error = json["error"] as? String {
                        //    self.showAlertWithText(error)
                        //}
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait()
    }
    
    override func viewDidLoad() {
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)

        super.viewDidLoad()
        keyTextField.text = "Введите название ключевика"
        let keyCell = UINib(nibName: "KeyCollectionCell",
                                  bundle: nil)
        self.keysCollection.register(keyCell,
                                      forCellWithReuseIdentifier: "KeyCollectionCell")
        keysCollection.delegate = self
        keysCollection.dataSource = self
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if isGood {
            if keyTextField.text != "" {
                goodKeys.append(keyTextField.text ?? "")
            }
            keys = goodKeys
            keysCollection.reloadData()
            addToGoodKeys(goodKeys)
        } else {
            if keyTextField.text != "" {
                badKeys.append(keyTextField.text ?? "")
            }
            keys = badKeys
            keysCollection.reloadData()
            addToBadKeys(badKeys)
        }
        keyTextField.text = ""
    }
    
    func addToGoodKeys(_ array: [String]){
        
        var valueString = "["
        
        for number in 0..<array.count {
            valueString += "{\"value\": \"\(array[number])\"}, "
            if number == array.count - 1 {
                valueString += "{\"value\": \"\(array[number])\"}]"
            }
        }

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "keyOne",
            "value": valueString,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/add-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }
    
    func addToBadKeys(_ array: [String]){
        
        var valueString = "["
        
        for number in 0..<array.count {
            valueString += "{\"value\": \"\(array[number])\"}, "
            if number == array.count - 1 {
                valueString += "{\"value\": \"\(array[number])\"}]"
            }
        }

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "keyMinus",
            "value": valueString,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/add-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()

    }

    
    @IBAction func goodKeysTapped(_ sender: Any) {
        negativeView.isHidden = true
        positiveView.isHidden = false
        isGood = true
        positiveButton.setTitleColor(UIColor.black, for: .normal)
        negativeButton.setTitleColor(UIColor.gray, for: .normal)
        keys = goodKeys
        keysCollection.reloadData()
    }
    
    @IBAction func badKeysTapped(_ sender: Any) {
        negativeView.isHidden = false
        positiveView.isHidden = true
        isGood = false
        positiveButton.setTitleColor(UIColor.gray, for: .normal)
        negativeButton.setTitleColor(UIColor.black, for: .normal)
        keys = badKeys
        keysCollection.reloadData()
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "Очистить список заявок", preferredStyle: .alert)
        let noAction = UIAlertAction(title: "Нет", style: .cancel) { [self] (UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
            
        }
        let clearAction = UIAlertAction(title: "Очистить", style: .default) { [self] (UIAlertAction) in
            goodKeys.removeAll()
            badKeys.removeAll()
            keys.removeAll()
            keysCollection.reloadData()
            self.dismiss(animated: true, completion: nil)
            deleteAll()
        }
        alert.addAction(noAction)
        alert.addAction(clearAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteAll(){

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "keyOne",
            "value": "[]",
            "type": "text"
          ],
          [
            "key": "keyMinus",
            "value": "[]",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/add-key")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
    
    var isGood = true
    
    var goodKeys = [String]()
    var badKeys = [String]()
    
    var keys = [String]()
}

extension KeysViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, KeyDelegate {
    func deleteProduct(cell: KeyCollectionCell) {
        let cellIndex = self.keysCollection.indexPath(for: cell)!.row
        if isGood {
            goodKeys.remove(at: cellIndex)
            keys = goodKeys
            addToGoodKeys(goodKeys)
        } else {
            badKeys.remove(at: cellIndex)
            keys = badKeys
            addToBadKeys(badKeys)
        }
        keysCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return keys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = (collectionView.dequeueReusableCell(withReuseIdentifier: "KeyCollectionCell", for: indexPath) as? KeyCollectionCell)!
        cell.delegate = self
        cell.keyTF.text = keys[indexPath.row]
        cell.layer.cornerRadius = 8
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width - 60
        return CGSize(width: width, height: 40)
    }
}
