//
//  CodeViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 29.01.2021.
//

import UIKit

class CodeViewController: UIViewController {
    
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var codeTF: UITextField!
    
    @IBOutlet weak var codeHeight: NSLayoutConstraint!
    
    @IBOutlet weak var sendButton: UIButton!
    
    
    override func viewDidLoad() {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
        super.viewDidLoad()
        codeHeight.constant = 0
        phoneTF.text = "Телефон"
        codeTF.text = "Код"
        phoneTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        codeTF.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        sendButton.layer.cornerRadius = 8
        phoneTF.delegate = self
        codeTF.delegate = self
    }
    
    var canSend = false
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text != code {
            canSend = false
            textField.layer.borderWidth = 0.5
            textField.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
        } else {
            canSend = true
            textField.layer.borderWidth = 0
        }
    }
    
    var isCodeVisible = false
    
    var code = ""
    
    @IBAction func sendTapped(_ sender: Any) {
        if isCodeVisible {
            if canSend {
                let vc = storyboard?.instantiateViewController(identifier: "NewPasswordViewController") as! NewPasswordViewController
                vc.phone = phoneTF.text ?? "default phone"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            
            var semaphore = DispatchSemaphore (value: 0)

            let parameters = [
              [
                "key": "login",
                "value": phoneTF.text ?? "default phone",
                "type": "text"
              ],
              [
                "key": "code",
                "value": "true",
                "type": "text"
              ]] as [[String : Any]]

            let boundary = "Boundary-\(UUID().uuidString)"
            var body = ""
            var error: Error? = nil
            for param in parameters {
              if param["disabled"] == nil {
                let paramName = param["key"]!
                body += "--\(boundary)\r\n"
                body += "Content-Disposition:form-data; name=\"\(paramName)\""
                if param["contentType"] != nil {
                  body += "\r\nContent-Type: \(param["contentType"] as! String)"
                }
                let paramType = param["type"] as! String
                if paramType == "text" {
                  let paramValue = param["value"] as! String
                  body += "\r\n\r\n\(paramValue)\r\n"
                } else {
                  let paramSrc = param["src"] as! String
                  let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
                  let fileContent = String(data: fileData!, encoding: .utf8)!
                  body += "; filename=\"\(paramSrc)\"\r\n"
                    + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
                }
              }
            }
            body += "--\(boundary)--\r\n";
            let postData = body.data(using: .utf8)

            var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/new-pas")!,timeoutInterval: Double.infinity)
            request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

            request.httpMethod = "POST"
            request.httpBody = postData

            let task = URLSession.shared.dataTask(with: request) { data, response, error in
              guard let data = data else {
                print(String(describing: error))
                semaphore.signal()
                return
              }
                DispatchQueue.main.async { [self] () -> Void in
                do {
                    
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            print(json)
                            if json["error"] == nil {
                                self.code = json["code"] as! String
                                isCodeVisible = true
                                codeHeight.constant = 45
                                print(code)
                            } else {
                                //self.showAlertWithText(json["error"] as! String)
                            }
                        // try to read out a string array
                        //if let error = json["error"] as? String {
                        //    self.showAlertWithText(error)
                        //}
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
                }
              semaphore.signal()
            }

            task.resume()
            semaphore.wait()
            
            
            
        }
    }
    
    
}
