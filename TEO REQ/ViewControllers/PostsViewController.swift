//
//  PostsViewController.swift
//  TEO REQ
//
//  Created by Святослав Шевченко on 30.01.2021.
//

import UIKit

class PostsViewController: UIViewController {
    
    struct Post {
        var id: String
        var name: String
        var username: String
        var subscribers_count: String
        var text: String
        var url: String
        var img: String
        var text_id: String
    }
    
    var allPosts = [Post]()
    var goodPosts = [Post]()
    var badPosts = [Post]()
    
    @IBOutlet weak var allPostsButton: UIButton!
    @IBOutlet weak var goodPostsButton: UIButton!
    @IBOutlet weak var badPostsButton: UIButton!
    @IBOutlet weak var postsTable: UITableView!
    
    @IBOutlet weak var allPostsView: UIView!
    @IBOutlet weak var goodPostsView: UIView!
    @IBOutlet weak var badPostsView: UIView!
    
    
    @IBOutlet weak var deleteButton: UIButton!
    
    var refreshControl = UIRefreshControl()
    
    @IBAction func deleteTapped(_ sender: Any) {
        
        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "status",
            "value": "\(postsStatus)",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/del-req")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        allPosts.removeAll()
        goodPosts.removeAll()
        badPosts.removeAll()
        allPostsButton.setTitleColor(UIColor.black, for: .normal)
        goodPostsButton.setTitleColor(UIColor.gray, for: .normal)
        badPostsButton.setTitleColor(UIColor.gray, for: .normal)
        goodPostsView.isHidden = true
        allPostsView.isHidden = false
        badPostsView.isHidden = true
        getDataAll()
        getDataGood()
        getDataBad()
    }
    
    
    override func viewDidLoad() {
        //refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        postsTable.addSubview(refreshControl)
        super.viewDidLoad()
        print(allPosts)
        //deleteButton.layer.borderWidth = 0.5
        //deleteButton.layer.borderColor = CGColor(red: 0, green: 0, blue: 0, alpha: 1)
        deleteButton.layer.cornerRadius = 8
        postsTable.tableFooterView = UIView(frame: .zero)
        postsTable.delegate = self
        postsTable.dataSource = self
        let postCell = UINib(nibName: "PostTableCell", bundle: nil)
        self.postsTable.register(postCell, forCellReuseIdentifier: "PostTableCell")
    }
    
    @objc func refresh(_ sender: AnyObject) {
        //allPosts.removeAll()
        //goodPosts.removeAll()
        //badPosts.removeAll()
        getDataAll()
        getDataGood()
        getDataBad()
        postsStatus = 0
        allPostsButton.setTitleColor(UIColor.black, for: .normal)
        goodPostsButton.setTitleColor(UIColor.gray, for: .normal)
        badPostsButton.setTitleColor(UIColor.gray, for: .normal)
        goodPostsView.isHidden = true
        allPostsView.isHidden = false
        badPostsView.isHidden = true
        if allPosts.isEmpty {
            postsTable.isHidden = true
            deleteButton.isHidden = true
        } else {
            postsTable.isHidden = false
            deleteButton.isHidden = false
            posts = allPosts
            postsTable.reloadData()
        }
        refreshControl.endRefreshing()
    }
    
    func getDataAll(){
        //allPosts.removeAll()
        var semaphore = DispatchSemaphore (value: 0)

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "page",
            "value": "1",
            "type": "text"
          ],
          [
            "key": "status",
            "value": "0",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/list")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            semaphore.signal()
            return
          }
          //print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                        if json.isEmpty {
                            postsTable.isHidden = true
                            deleteButton.isHidden = true
                        } else {
                            for number in 0..<json.count  {
                                if let telegram = json[number]["telegramChanel"] as? [String: Any] {
                                    allPosts.append(Post(id: json[number]["id"] as! String, name: json[number]["name"] as! String, username: json[number]["username"] as? String ?? "", subscribers_count: json[number]["subscribers_count"] as? String ?? "", text: json[number]["text"] as! String, url: telegram["url"] as! String, img: telegram["img"] as? String ?? "", text_id: json[number]["text_id"] as? String ?? ""))
                                }
                            }
                            posts = allPosts
                            postsTable.reloadData()
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
    }
    
    func getDataGood(){

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "page",
            "value": "1",
            "type": "text"
          ],
          [
            "key": "status",
            "value": "1",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/list")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          //print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                        if json.isEmpty {
                        } else {
                            for number in 0..<json.count  {
                                if let telegram = json[number]["telegramChanel"] as? [String: Any] {
                                goodPosts.append(Post(id: json[number]["id"] as! String, name: json[number]["name"] as! String, username: json[number]["username"] as? String ?? "", subscribers_count: json[number]["subscribers_count"] as? String ?? "", text: json[number]["text"] as! String, url: telegram["url"] as! String, img: telegram["img"] as? String ?? "", text_id: json[number]["text_id"] as? String ?? ""))
                                }
                            }
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }

        task.resume()
    }
    
    func getDataBad(){

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "page",
            "value": "1",
            "type": "text"
          ],
          [
            "key": "status",
            "value": "2",
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/list")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          //print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async { [self] () -> Void in
                do {
                    
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                        if json.isEmpty {
                        } else {
                            for number in 0..<json.count  {
                                if let telegram = json[number]["telegramChanel"] as? [String: Any] {
                                badPosts.append(Post(id: json[number]["id"] as! String, name: json[number]["name"] as! String, username: json[number]["username"] as? String ?? "", subscribers_count: json[number]["subscribers_count"] as? String ?? "", text: json[number]["text"] as! String, url: telegram["url"] as! String, img: telegram["img"] as? String ?? "", text_id: json[number]["text_id"] as? String ?? ""))
                                }
                            }
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }

        task.resume()
    }
    
    var postsStatus = 0
    
    @IBAction func allPostsTapped(_ sender: Any) {
        postsStatus = 0
        allPostsButton.setTitleColor(UIColor.black, for: .normal)
        goodPostsButton.setTitleColor(UIColor.gray, for: .normal)
        badPostsButton.setTitleColor(UIColor.gray, for: .normal)
        if allPosts.isEmpty {
            postsTable.isHidden = true
            deleteButton.isHidden = true
        } else {
            postsTable.isHidden = false
            deleteButton.isHidden = false
            posts = allPosts
            postsTable.reloadData()
        }
        goodPostsView.isHidden = true
        allPostsView.isHidden = false
        badPostsView.isHidden = true
    }
    
    @IBAction func goodPostsTapped(_ sender: Any) {
        postsStatus = 1
        allPostsButton.setTitleColor(UIColor.gray, for: .normal)
        goodPostsButton.setTitleColor(UIColor.black, for: .normal)
        badPostsButton.setTitleColor(UIColor.gray, for: .normal)
        if goodPosts.isEmpty {
            postsTable.isHidden = true
            deleteButton.isHidden = true
        } else {
            postsTable.isHidden = false
            deleteButton.isHidden = false
            posts = goodPosts
            postsTable.reloadData()
        }
        goodPostsView.isHidden = false
        allPostsView.isHidden = true
        badPostsView.isHidden = true
    }
    
    @IBAction func badPostsTapped(_ sender: Any) {
        postsStatus = 2
        allPostsButton.setTitleColor(UIColor.gray, for: .normal)
        goodPostsButton.setTitleColor(UIColor.gray, for: .normal)
        badPostsButton.setTitleColor(UIColor.black, for: .normal)
        if badPosts.isEmpty {
            postsTable.isHidden = true
            deleteButton.isHidden = true
        } else {
            postsTable.isHidden = false
            deleteButton.isHidden = false
            posts = badPosts
            postsTable.reloadData()
        }
        goodPostsView.isHidden = true
        allPostsView.isHidden = true
        badPostsView.isHidden = false
    }
    
    var posts = [Post]()
    
    
}

extension PostsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableCell") as! PostTableCell
        if postsStatus == 0 {
            cell.layer.borderWidth = 0
        }
        if postsStatus == 1 {
            cell.layer.borderWidth = 0.5
            cell.layer.borderColor = CGColor(red: 0, green: 1, blue: 0, alpha: 1)
        }
        if postsStatus == 2 {
            cell.layer.borderWidth = 0.5
            cell.layer.borderColor = CGColor(red: 1, green: 0, blue: 0, alpha: 1)
        }
        DispatchQueue.main.async { [self] in
            cell.id = indexPath.row
            cell.textPost = posts[indexPath.row].text
        cell.nameLabel.text = posts[indexPath.row].name
        cell.textPostLabel.text = posts[indexPath.row].text
        cell.nickLabel.text = "@" + posts[indexPath.row].username
        cell.subsLabel.text = posts[indexPath.row].subscribers_count + " чел."
        }
        do {
            let url = URL(string: posts[indexPath.row].img) ?? URL(string: "https://cdn.icon-icons.com/icons2/1812/PNG/512/4213460-account-avatar-head-person-profile-user_115386.png")
            let data = try Data(contentsOf: url!)
            DispatchQueue.main.async {
            cell.chanelImage.image = UIImage(data: data)
            }
        }
        catch{
            print(error)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 197
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = storyboard?.instantiateViewController(identifier: "BigPostViewController") as! BigPostViewController
        vc.delegate = self
        vc.imageLink = posts[indexPath.row].img
        vc.nick = posts[indexPath.row].username
        vc.name = posts[indexPath.row].name
        vc.subs = posts[indexPath.row].subscribers_count
        vc.textPost = posts[indexPath.row].text
        vc.id = indexPath.row
        vc.idPost = posts[indexPath.row].id
        vc.openID = posts[indexPath.row].text_id
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let minus = UIContextualAction(style: .destructive, title: "Отклонить") { [self] (action, view, success: (Bool) -> Void) in
            tableView.deleteRows(at:[indexPath], with: .fade)
            if postsStatus == 0 {
                allPosts.remove(at: indexPath.row)
            }
            if postsStatus == 1 {
                goodPosts.remove(at: indexPath.row)
            }
            if postsStatus == 2 {
                badPosts.remove(at: indexPath.row)
            }
            addToBad(indexPath.row)
            success(true)
        }
        
        return UISwipeActionsConfiguration(actions: [minus])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let plus = UIContextualAction(style: .normal, title: "Одобрить") { [self] (action, view, success: (Bool) -> Void) in
            tableView.deleteRows(at:[indexPath], with: .fade)
            if postsStatus == 0 {
                allPosts.remove(at: indexPath.row)
            }
            if postsStatus == 1 {
                goodPosts.remove(at: indexPath.row)
            }
            if postsStatus == 2 {
                badPosts.remove(at: indexPath.row)
            }
            addToGood(indexPath.row)
            success(true)
        }
        plus.backgroundColor = UIColor(red: 70/255, green: 230/255, blue: 172/255, alpha: 1)
        return UISwipeActionsConfiguration(actions: [plus])
    }
    
    func addToGood(_ index: Int){

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "status",
            "value": "1",
            "type": "text"
          ],
          [
            "key": "id",
            "value": posts[index].id,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/change-req")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
    
    func addToBad(_ index: Int){

        let parameters = [
          [
            "key": "token",
            "value": UserDefaults.standard.string(forKey: "token") ?? "",
            "type": "text"
          ],
          [
            "key": "status",
            "value": "2",
            "type": "text"
          ],
          [
            "key": "id",
            "value": posts[index].id,
            "type": "text"
          ]] as [[String : Any]]

        let boundary = "Boundary-\(UUID().uuidString)"
        var body = ""
        var error: Error? = nil
        for param in parameters {
          if param["disabled"] == nil {
            let paramName = param["key"]!
            body += "--\(boundary)\r\n"
            body += "Content-Disposition:form-data; name=\"\(paramName)\""
            if param["contentType"] != nil {
              body += "\r\nContent-Type: \(param["contentType"] as! String)"
            }
            let paramType = param["type"] as! String
            if paramType == "text" {
              let paramValue = param["value"] as! String
              body += "\r\n\r\n\(paramValue)\r\n"
            } else {
              let paramSrc = param["src"] as! String
              let fileData = try? NSData(contentsOfFile:paramSrc, options:[]) as Data
              let fileContent = String(data: fileData!, encoding: .utf8)!
              body += "; filename=\"\(paramSrc)\"\r\n"
                + "Content-Type: \"content-type header\"\r\n\r\n\(fileContent)\r\n"
            }
          }
        }
        body += "--\(boundary)--\r\n";
        let postData = body.data(using: .utf8)

        var request = URLRequest(url: URL(string: "https://app.teo-req.ru/api/v1/change-req")!,timeoutInterval: Double.infinity)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")

        request.httpMethod = "POST"
        request.httpBody = postData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        }

        task.resume()
    }
}

extension PostsViewController: PostDelegate {
    func deleteProduct(id: Int) {
        if postsStatus == 0 {
            allPosts.remove(at: id)
            posts = allPosts
            postsTable.reloadData()
            
        }
        if postsStatus == 1 {
            goodPosts.remove(at: id)
            posts = goodPosts
            postsTable.reloadData()
        }
        if postsStatus == 2 {
            badPosts.remove(at: id)
            posts = badPosts
            postsTable.reloadData()
        }
        if posts.isEmpty {
            postsTable.isHidden = true
            deleteButton.isHidden = true
        }
    }
}

